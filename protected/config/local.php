<?php
/**
 * Локальная переопределяющая конфигурация
 */

defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);

return array(

  'modules'=>(!YII_DEBUG ? array() : array(
    'gii'=>array(
      'password' => '123',
    ),
  )),

  'components' => array(
    'db' => array(
      'connectionString' => 'mysql:host=localhost;dbname=testwork',
      'username' =>  'root',
      'password' => '',
      'emulatePrepare' => true,
      'charset' => 'utf8',
      'schemaCachingDuration'=>3600,
      'enableProfiling' => true,
      'enableParamLogging' => true,
    ),

    'clientScript' => array(
      'combineCss' => true,
      'combineJs' => true,
    ),

    'log' => array(
      'class'=>'CLogRouter',
        'routes'=>array(
           'sql_table_news' => array(
          'class'=>'ygin.components.SqlTableLogRoute',
          'logOnlyQuery' => true,
          'includedQueries' => array(
            '~INSERT~',
            '~CREATE~',
            '~UPDATE~',
            '~DELETE~',
          ),
          'tablesLog' => array(
            'pr_comment',
            'pr_news',
          ),
          'excludedQueries' => array(
            '~SHOW CREATE TABLE~',
            '~^SELECT~',
            '~SHOW FULL COLUMNS~',
          ),
          'nameLogFile' => 'sql.log',
          'enabled' => YII_DEBUG,
        ),


  ),
    ),),
);


